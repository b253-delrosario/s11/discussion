// Built-in Imports
import {Fragment} from "react";

// Downloaded Package Modules Imports
import {Container} from "react-bootstrap";

import "./App.css";

// (user-defined) Components Imports (alphabetical or according to file structure)
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home'
// To be executed in Home page
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';

function App() {
  return (

    /*
      - JSX syntax requires that components should always have closing tags.
      - In the example above, since the "AppNavbar" component does not require any text to be placed in between, we add a "/" at the end to signify that they are self-closing tags.
    */

    /*
      - In React JS, multiple components rendered in a single component should be wrapped in a parent component.
      - Not doing so will return an error in our application.
      - The "Fragment" component ensures that this error can be prevented.
    */

  <Fragment>
    <AppNavbar />
    <Container>
      <Home/>
       {/*<Banner />
       <Highlights />*/}
    </Container>
  </Fragment>

  // Or you can use the empty tags as a parent component
  // <>
  //   <AppNavbar />
  //   <Banner />
  // </>

  // and the div tags as a parent component
  // <div>
  //   <AppNavbar />
  //   <Banner />
  // </div>

  );
}

export default App;
