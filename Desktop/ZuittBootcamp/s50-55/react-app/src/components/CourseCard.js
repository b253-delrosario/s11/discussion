import {Row, Col, Card, Button} from "react-bootstrap";

export default function CourseCard() {
	return(
		<Row className="mt-3 mb-3">
			<Col md={12}>
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h4>Sample Course</h4>
					</Card.Title>
					<Card.Text>
						<div><strong>Description:</strong></div>
						<div>This is a sample course offering.</div>
						<br/>
						<div><strong>Price:</strong></div>
						<div>Php 40,000</div>
					</Card.Text>
					<Button variant="primary">Enroll</Button>
				</Card.Body>
		   	</Card>
		   	</Col>
		</Row>
		
	);
}