const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for user registration (POST)
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});





// Route for retrieving all users (GET - admin only)
router.get("/all", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	// Admin
	if(userData.isAdmin) {
		userController.getAllUsers().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} 

	// Non-admin
	else {
		res.send("You are not authorized to view this page.")
	}
});





// Route for user authentication/login (POST)
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});





// Route for retrieving user details (GET)
router.get("/:id/user-details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	// Admin and Non-admin who matches the ID
	if(userData.isAdmin || userData.id === req.params.id) {
		userController.getUserDetails(req.params.id).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	}

	// Non-admin who doesn't match the ID
	else {
		res.send("You are not authorized to view this page.")
	}
});





// Route for setting a user to admin (PATCH)
router.patch("/:id/admin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	// Admin
	if(userData.isAdmin) {
		userController.setAsAdmin(req.params.id).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	}

	// Non-admin
	else {
		res.send("You are not authorized to view this page.")
	}
});







module.exports = router;