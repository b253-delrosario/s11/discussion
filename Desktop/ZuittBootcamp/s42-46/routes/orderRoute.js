const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const productController = require("../controllers/productController");
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for creating orders
router.post("/checkout", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

	let data = {
            userId: userData.id,
            courseId: req.body.productId,
            isAdmin: userData.isAdmin
        }

    if (!data.isAdmin) {
    	orderController.addOrder({user:req.body.userId, product:req.body.productId, quantity:req.body.quantity}).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
    	res.send("You are not authorized to add order.");
    }
});




// Route for retrieving all orders
router.get("/", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

	let data = {
            userId: userData.id,
            isAdmin: userData.isAdmin
        }

    if (data.isAdmin) {
    	orderController.getAllOrders().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
    	res.send("You are not authorized to do such action.");
    }
});






// Route for retrieving all orders
router.post("/user-orders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

	let data = {
            userId: userData.id,
            isAdmin: userData.isAdmin
        }

    if (data.isAdmin) {
    	orderController.getUserOrders(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
    	res.send("You are not authorized to do such action.");
    }
});


module.exports = router;
