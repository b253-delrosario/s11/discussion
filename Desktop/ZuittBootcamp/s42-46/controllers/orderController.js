const auth = require("../auth");
const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");

// ============ ORDER CREATION BY USER ============ //
module.exports.addOrder = (reqBody) => {
	return User.findOne({_id:reqBody.user}).then(user => {
		if (user == null) {
			return "User does not exist."
		}

		else {
			return Product.findOne({_id:reqBody.product}).then(result => {

				if (!result) {
					return "Product does not exist."
				}

				if (!result.isActive) {
					return "Product is no longer available."
				}

				else {
					const totalAmount = result.price * reqBody.quantity;

					let order = new Order ({
						userId:reqBody.user,
						products: [{
							productId:reqBody.product,
							quantity: reqBody.quantity
						}],
						totalAmount
					});

					return order.save().then(order => {
						if (order) {
							return `Hi! Your order was placed successfully. Order ID: ${order._id}`
						}

						else {
							return "Order was not placed."
						}
					})
				}
				
			})
				
		}
	}).catch(err => {
		return err;
	})
}



module.exports.getAllOrders = () => {
	return Order.find().populate('products.productId').then(orders => {
		const data = orders.map(order => {
			return {
				"id": order.id,
				"userId": order.userId,
				"productId": order.products[0].productId._id,
				"product": order.products[0].productId.name,
				"quantity": order.products[0].quantity,
				"totalAmount": order.totalAmount,
				"status": order.status
			};
		});
		return data;
	}).catch(err => err);
};


module.exports.getUserOrders = (reqBody) => {
	return Order.find({userId: reqBody.userId}).populate('products.productId').then(orders => {
		const data = orders.map(order => {
			return {
				"id": order.id,
				"productId": order.products[0].productId._id,
				"product": order.products[0].productId.name,
				"quantity": order.products[0].quantity,
				"totalAmount": order.totalAmount,
				"status": order.status
			};
		});
		return data;
	}).catch(err => err);
};

