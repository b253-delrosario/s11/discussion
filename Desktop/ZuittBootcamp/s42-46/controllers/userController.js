const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");


// ============ USER REGISTRATION ============ //
module.exports.registerUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {

		// No key-value pair
		if (reqBody.firstName == null && reqBody.lastName == null && reqBody.email == null && reqBody.password == null) { 
			return {
				status: "Registration failed.",
				error: {
					firstName: "First name is required.",
					lastName: "Last name is required.",
					email: "Email is required.",
					password: "Password is required.",
				}
			} 
		} 

		// Missing key-value pair (firstName)
		if (reqBody.firstName == null || reqBody.lastName == null || reqBody.email == null || reqBody.password == null) { 
			return {
				status: "Registration failed.",
				error: {
					firstName: "First name is required.",
					lastName: "Last name is required.",
					email: "Email is required.",
					password: "Password is required."
				}
			} 
		} 

		// No data provided
		else if (reqBody.firstName == "" && reqBody.lastName == "" && reqBody.email == "" && reqBody.password == "") { 
			return {
				status: "Registration failed.",
				error: {
					firstName: "First name is required.",
					lastName: "Last name is required.",
					email: "Email is required.",
					password: "Password is required."
				}
			} 
		}



		// No first & last name, and email provided
		else if (reqBody.firstName === "" && reqBody.lastName === "" && reqBody.email === "") {
			return { 
				status: "Registration failed.",
				error: {
					firstName: "First name is required.",
					lastName: "Last name is required.",
					email: "Email is required.",
				}
			}
		}

		// No first & last name, and password provided
		else if (reqBody.firstName === "" && reqBody.lastName === "" && reqBody.password === "") {
			return { 
				status: "Registration failed.",
				error: {
					firstName: "First name is required.",
					lastName: "Last name is required.",
					password: "Password is required.",
				}
			}
		}

		// No last name, email and password provided
		else if (reqBody.firstName === "" && reqBody.email === "" && reqBody.password === "") {
			return { 
				status: "Registration failed.",
				error: {
					firstName: "First name is required.",
					email: "Email is required.",
					password: "Password is required.",
				}
			}
		}

		// No last name, email and password provided
		else if (reqBody.lastName === "" && reqBody.email === "" && reqBody.password === "") {
			return { 
				status: "Registration failed.",
				error: {
					lastName: "Last name is required.",
					email: "Email is required.",
					password: "Password is required.",
				}
			}
		}

		// No last name, and password provided
		else if (reqBody.lastName === "" && reqBody.email === "" && reqBody.password === "") {
			return { 
				status: "Registration failed.",
				error: {
					firstName: "First name is required.",
					email: "Email is required.",
					password: "Password is required.",
				}
			}
		}

		// No first and lastname provided
		else if (reqBody.firstName === "" && reqBody.lastName === "") {
			return { 
				status: "Registration failed.",
				error: {
					firstName: "First name is required.",
					lastName: "Last name is required.",
				}
			}
		}

		// No first name provided
		else if (reqBody.firstName === "") {
			return { 
				status: "Registration failed.",
				error: "First name is required."
			}
		}

		// No last name provided
		else if (reqBody.lastName === "") {
			return {
				status: "Registration failed.",
				error: "Last name is required."
			}
		} 
		
		// No email and password provided
		else if (reqBody.email === "" && reqBody.password === "") {
			return {
				status: "Registration failed.",
				error: {
					email: "Email is required.",
					password: "Password is required."
				}
			}
		} 

		// No email provided
		else if (reqBody.email === "") {
			return {
				status: "Registration failed.",
				error: "Email is required.",
			}
		} 

		// No password provided
		else if (reqBody.password === "") {
			return {
				status: "Registration failed.",
				error: "Password is required."	
			}
		}

		// Provided an email that was already in use.
		else if (result != null && result.email == reqBody.email) {
			return {
				error: "The email you provided is already associated with an existing account."
			}
		} 

		// Filled out all mandatory fields and provided an email that is not currently in use.
		else { 
			let newUser = new User ({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				isAdmin: reqBody.isAdmin
			});

			return newUser.save().then(user => {

				// Successful Admin Registration
				if (user && user.isAdmin) {
					return "Hi " + reqBody.firstName +"! Admin registration was successful."
				} 

				// Successful User Registration
				else if (user && !user.isAdmin) {
					return "Hi " + reqBody.firstName + "! User registration was successful."
				}

				// Error
				else {
					return err
				}
			});
		}
	}).catch(err => err);
};





// ============ RETRIEVAL OF ALL USERS ============ //
module.exports.getAllUsers = () => {
	return User.find({}).then(result => result).catch(err => err);
};





// ============ USER AUTHENTICATION/LOGIN ============ //
module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {

		// No key-value pair
		if (reqBody.email == null && reqBody.password == null) {
    		return {
    			status: "Authentication failed.",
    			error: "Please enter your email and password."
    		}
    	}

    	// Missing key-value pair
		if (reqBody.email == null || reqBody.password == null) {
    		return {
    			status: "Authentication failed.",
    			error: "Please enter your email and password."
    		}
    	}

    	// No login credentials provided
		if (reqBody.email == "" && reqBody.password == "") {
    		return {
    			status: "Authentication failed.",
    			error: "Please enter your email and password."
    		}
    	}

		// No email entry
		else if (reqBody.email == "") {
    		return {
    			status: "Authentication failed.",
    			error: "Please enter your email."
    		}
    	}

    	// No password entry
		else if (reqBody.password == "") {
    		return {
    			status: "Authentication failed.",
    			error: "Please enter your password."
    		}
    	}

    	// USER DOES NOT EXIST
    	// Entered both an email and password but email is not connected to any existing account
 		else if (result == null) {
			return {
    			status: "Authentication failed.",
    			error: "The email you entered isn’t connected to an existing account."
    		} 
		}

		// USER EXISTS
		// Entered both an email and password and email is connected to an existing account
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// Correct password
			if (isPasswordCorrect) {
				return {
					status: "Authentication successful.",
					access: auth.createAccessToken(result)
				}
			} 

			// Incorrect password
			else {
				return {
					status: "Authentication failed.",
					error: "The password you’ve entered is incorrect."
    			}	
			}
		}
	}).catch(err => err);
};





// ============ RETRIEVAL OF USER'S DETAILS ============ //
// It can only be obtained by an admin or the user itself
module.exports.getUserDetails = (reqParams) => {
	return User.findById(reqParams).then(result => result).catch(err => err);
};





// ============ SETTING USER TO ADMIN ============ //
module.exports.setAsAdmin = (reqParams) => {
	let admin = {
		isAdmin: true
	};

	return User.findById(reqParams, admin).then(result => {
		if (result.isAdmin){
			return  {
				status: "User is already an admin."
			}
		}

		else {
			return User.findByIdAndUpdate(reqParams, admin, {new:true}).then(result => { 
				return {
					status: result.firstName + " is now an admin.",
						userData: {
							userId: result.id,
							firstName: result.firstName,
							lastName: result.lastName,
							email: result.email,
							isAdmin: result.isAdmin
						}
				}
			}).catch(err => err);
		}
	})
};